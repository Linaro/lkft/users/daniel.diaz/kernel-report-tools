#!/bin/bash

# $1: year
# $2: month
get_start_date() {
  date -d "$1-$2-01" +"%Y-%m-%d"
}

# $1: year
# $2: month
get_end_date() {
  year="$1"
  month="$2"

  month_padded="$(printf "%02d" "${month}")"
  next_year="${year}"
  next_month="$((month + 1))"
  if [ "${next_month}" -gt 12 ]; then
    next_month=1
    next_year="$((next_year + 1))"
  fi

  date -d "${next_year}-${next_month}-01 - 1 day" +"%Y-%m-%d"
}

# $1: type ("Author", "Reported-by", "Tested-by")
# $2: output file name
# $3: Git range
# Env: ${root_dir}
get_commit_ids() {
  type="$1"
  output="$2"
  shift; shift;
  git_range="$@"

  expr=""
  git_filter=""
  git_show_format="format:%b"
  case ${type,,} in
    "reported-by")
      expr="reported-.*by:"
      ;;
    "tested-by")
      expr="tested-.*by:"
      ;;
    "author")
      expr="^author:"
      git_filter="--author=linaro.org"
      git_show_format="short"
      ;;
  esac

  : > "${output}"
  #shellcheck disable=SC2154
  git rev-list "${git_range[@]}" ${git_filter} | while read -r commitid; do
    if git show --format="${git_show_format}" "${commitid}" | grep -i "${expr}" | grep -qf "${root_dir}/lkft-team-emails.txt"; then
      echo "${commitid}" >> "${output}"
      echo -n "."
    fi
  done
}

# $1: year
# $2: month
# $3: type ("Author", "Reported-by", "Tested-by")
get_commit_ids_by_date() {
  year="$1"
  month="$2"
  type="$3"

  start_date="$(get_start_date "${year}" "${month}")"
  end_date="$(get_end_date "${year}" "${month}")"

  month_padded="$(printf "%02d" "${month}")"
  echo -n "${year}-${month_padded}: "

  filename="report-${type,,}-${year}${month_padded}.txt"

  git_range=(--since="${start_date} 00:00:00" --until="${end_date} 23:59:59" --all)
  get_commit_ids "${type}" "${filename}" "${git_range}"
  echo
}

# $1: from release
# $2: up until release
# $3: type ("Author", "Reported-by", "Tested-by")
# Format of release versions: v4.19, v5.4, v5.9, v6.0, etc.
get_commit_ids_by_release() {
  from_release="$1"
  until_release="$2"
  type="$3"

  echo -n "${until_release}: "

  filename="report-${type,,}-${until_release}.txt"

  git_range=("${from_release}..${until_release}")
  get_commit_ids "${type}" "${filename}" "${git_range}"
  echo
}
