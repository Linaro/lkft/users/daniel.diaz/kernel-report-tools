#!/bin/bash

set -e

root_dir="$(dirname "$(readlink -e "$0")")"
. "${root_dir}/lib.sh"

usage() {
  echo "Usage:"
  echo "  $0 --date <year> <month> <report_type>"
  echo "or:"
  echo "  $0 --release <from_tag> <until_tag> <report_type>"
  echo ""
  echo "Report type is one of: 'Author', 'Reported-by', 'Tested-by'"
}

if [ $# -lt 4 ]; then
  usage
  exit 1
fi

report_format="$1"
shift

type="$3"
case ${type} in
  Author) : ;;
  Reported-by) : ;;
  Tested-by) : ;;
  *)
    usage
    exit 1
    ;;
esac

if [ "${report_format}" = "--date" ]; then
  year="$1"
  month="$2"
  get_commit_ids_by_date "${year}" "${month}" "${type}"
elif [ "${report_format}" = "--release" ]; then
  from_tag="$1"
  until_tag="$2"
  get_commit_ids_by_release "${from_tag}" "${until_tag}" "${type}"
else
  usage
  exit 1
fi
